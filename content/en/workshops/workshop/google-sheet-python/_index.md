+++
title = 'Google Sheet Python'
date = 2024-01-12T15:18:24-08:00
draft = false
+++

This is workshop is based on:  
- https://docs.gspread.org/en/latest/index.html#
- https://lcalcagni.medium.com/how-to-manipulate-google-spreadsheets-using-python-b15657e6ed0d
- https://www.youtube.com/watch?v=sVURhxyc6jE

In this workshop, I will explain how can we manipulate google sheets using Python.  

- Step 1: Sign up for a google account 
- Step 2: Create a Google Service Account
     - 2.1 Create a new project on Google Cloud Platform
     - 2.2 Enable the required APIs
     - 2.3 Manage Google Sheets API credentials
- Step 3: Share your Google Sheet with your Service Account
- Step 4: Manipulate your Google Sheet with Python
