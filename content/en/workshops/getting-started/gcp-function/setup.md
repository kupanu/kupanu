+++
title = 'Setup'
date = 2024-03-20T17:58:16-07:00
draft = false
weight = 1
+++

### Before you begin

1. In the Google Cloud console, on the project selector page, select or [create a Google Cloud project](https://cloud.google.com/resource-manager/docs/creating-managing-projects).
2. Make sure that billing is enabled for your Google Cloud project.
3. Enable the Cloud Functions, Cloud Build, Artifact Registry, Cloud Run, and Logging APIs.
4. Install the Google Cloud CLI.

- To initialize the gcloud CLI, run the following command:  
`gcloud init` *Don't run this if you already have the gcloud setup*

- Log of example code:  
`git clone https://github.com/GoogleCloudPlatform/python-docs-samples.git`

