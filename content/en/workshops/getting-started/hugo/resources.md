+++
title = 'Resources'
date = 2023-12-05T15:45:17-08:00
weight = 6
draft = false
+++
### Themes: 
#### Good for company: 
- https://themes.gohugo.io/themes/hugo-universal-theme/
- https://themes.gohugo.io/themes/gohugo-theme-ananke/
- https://themes.gohugo.io/themes/hugo-theme-massively/
#### Good for documentation: 
- https://themes.gohugo.io/themes/docsy/
#### Nice Effect 
- https://themes.gohugo.io/themes/hugo-theme-air/
#### Simple 
- https://themes.gohugo.io/themes/hugo-theme-monochrome/
- https://themes.gohugo.io/themes/hugo-papermod/
#### A3ir.com theme options: (private) 
- https://themes.gohugo.io/themes/minimo/
- https://themes.gohugo.io/themes/cupper-hugo-theme/
#### Thepragmaticai.com (public)
- https://themes.gohugo.io/themes/gohugo-theme-ananke/

#### Company
- https://themes.gohugo.io/themes/up-business-theme/
- https://themes.gohugo.io/themes/hugo-fresh/