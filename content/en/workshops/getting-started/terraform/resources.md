+++
title = "Resources"
date = 2023-12-28T11:15:29-08:00
weight = 5
+++

https://www.terraform.io

Intro: https://developer.hashicorp.com/terraform/intro 

- Terraform Main Concepts 
- Object Types 
    - Providers - AWS 
    - Resources - VPS 
	- Data Sources - Current list of AZ in AWS  

https://developer.hashicorp.com/terraform/intro/core-workflow

Workflow:
(Write) Init - Plan - Apply - Destroy 

https://app.pluralsight.com/course-player?clipId=fd1c5401-511b-4f0a-ba6d-2aac0d237bac


- Organize code:  
https://medium.com/xebia-engineering/best-practices-to-create-organize-terraform-code-for-aws-2f4162525a1a  
https://github.com/GoogleCloudPlatform/terraformer  
A CLI tool that generates tf/json and tfstate files based on existing infrastructure (reverse Terraform).  
