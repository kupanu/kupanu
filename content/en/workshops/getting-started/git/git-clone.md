+++
title = 'Git Clone'
date = 2024-01-13T18:00:18-08:00
draft = false
+++

You can also upload existing files from your computer using the instructions below.

Git global setup
git config --global user.name "Pragmatic X"
git config --global user.email "pragmastix@gmail.com"
```
    Create a new repository
    git clone git@gitlab.com:pragmaticx/codesnippet.git
    cd codesnippet
    git switch --create main
    touch README.md
    git add README.md
    git commit -m "add README"
    git push --set-upstream origin main
```
    Push an existing folder
    cd existing_folder
    git init --initial-branch=main
    git remote add origin git@gitlab.com:pragmaticx/codesnippet.git
    git add .
    git commit -m "Initial commit"
    git push --set-upstream origin main
```
```
    Push an existing Git repository
    cd existing_repo
    git remote rename origin old-origin
    git remote add origin git@gitlab.com:pragmaticx/codesnippet.git
    git push --set-upstream origin --all
    git push --set-upstream origin --tags
```