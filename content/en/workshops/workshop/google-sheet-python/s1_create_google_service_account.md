+++
title = 'Create Google Service Account'
date = 2024-01-12T16:34:48-08:00
draft = false
+++

## Step 1: Create a Google Service Account
First, we will have to create a Service Account. This account will be used to make authorized API calls to Google Cloud Services. It is important to mention that to create a Service Account it is required to have a Google account first.
