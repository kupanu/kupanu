+++
title = 'Package'
date = 2024-01-15T16:23:13-08:00
draft = false
+++
### Introduction 

Creating a python module and package and using it locally.  
This link is a good resource: 
- https://realpython.com/python-import/ 
- https://realpython.com/python-modules-packages/

### Overview: 

Module is simple .py file.  
Package is a folder with group of .py files and `__init.py__`  like:   

    r2d2/
    │
    ├── bootstrap/
    │   ├── __init__.py
    │   └── database.py
    │   └── config.py
    │   └── project.py
    │
    ├── helpme/
    │   ├── __init__.py
    │   ├── organize.py
    │
    ├── hotel/
    │   ├── aareana.py
    │   └── mantra.py
    │   └── machester.py
    │
    └── __init__.py

However, the folder needs to be in PYTHONPATH so that the module can be used.  
PYTHONPATH can be set by `export PYTHONPATH="$HOME/repos/gitlab/chores/r2d2"`  
It can be tested by: 

```
import sys 
print(sys.path)
```

Let's create a package that will help me everyday works and call it r2d2


