+++
title = 'Getting Start'
date = 2023-12-30T14:38:17-08:00
draft = false
weight = 1
+++

Links:
- [Go Getting Start](https://go.dev/doc/tutorial/getting-started)
- [Tutorial](https://go.dev/doc/tutorial/)
- [W3 Schools](https://www.w3schools.com/go/index.php)
- [Example](https://gobyexample.com)
- [geeksforgeeks](https://www.geeksforgeeks.org/golang-tutorial-learn-go-programming-language/#)
- [GoLangPrograms](https://www.golangprograms.com/go-language.html)
- [Compile and Install](https://go.dev/doc/tutorial/compile-install)
- [DigitalOcean Tutorial](https://www.digitalocean.com/community/tutorial-series/how-to-code-in-go)
- [Build and Install Go](https://www.digitalocean.com/community/tutorials/how-to-build-and-install-go-programs) 

Steps: 
- Install go: https://go.dev/doc/install

#### Write some code 

1. Create a directory: 

    `mkcd hello`

2. Initialize:  
`go mod init example/hello`

3. Createa file: hello.go  
`code hello.go`

4. Add code: 
    ```
    package main
    import "fmt"

    func main() {
        fmt.Println("Hello, World!")
    }
    ```

5. Run Code:  
`go run .`

#### Call code in an external package


6. Install Module (like Pip in Python):  

    `go get rsc.io/quote`

7. After importing module add this to code:  

    ```
    package main
    import "fmt"
    import "rsc.io/quote"

    func main() {
        fmt.Println(quote.Go())
    }
    ```
8. Add new module requirements and sums.
    `go mod tody`
9. Run 
    `go run .`
10. Build 
    `go build .`
11. Execute 
    `./hello`