+++
title = 'Hugo Learn'
date = 2023-12-27T19:52:19-08:00
weight = 5
draft = false
+++

Moved to relearn: https://mcshelby.github.io/hugo-theme-relearn/index.html 
Main resource comes from:  https://learn.netlify.app/en/  
Installation: https://learn.netlify.app/en/basics/installation/  

### Create your project
Hugo provides a new command to create a new website.  
`hugo new site <new_project>`

### Install the theme

Install the **Hugo-theme-learn**  theme by following this documentation  
This theme’s repository is: https://github.com/matcornic/hugo-theme-learn.git  
Alternatively, you can download the theme as .zip file and extract it in the `themes` directory  

### Basic configuration

When building the website, you can set a theme by using --theme option. However, we suggest you modify the configuration file (config.toml) and set the theme as the default. You can also add the [outputs] section to enable the search functionality.

```
# Change the default theme to be use when building the site with Hugo
theme = "hugo-theme-learn"

# For search functionality
[outputs]
home = [ "HTML", "RSS", "JSON"]
```

### Create your first chapter page

Chapters are pages that contain other child pages. It has a special layout style and usually just contains a chapter name, the title and a brief abstract of the section.

```
### Chapter 1

# Basics

Discover what this Hugo theme is all about and the core concepts behind it.
```

**Hugo-theme-learn** provides archetypes to create skeletons for your website. Begin by creating your first chapter page with the following command  

```
hugo new --kind chapter basics/_index.md
```

### Create your first content pages

Then, create content pages inside the previously created chapter. Here are two ways to create content in the chapter:

```
hugo new basics/first-content.md
hugo new basics/second-content/_index.md
```

A good Example:  
https://github.com/aws-containers/ecsworkshop/tree/main



