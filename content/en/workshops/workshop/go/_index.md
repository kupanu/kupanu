+++
title = 'Go-Workshop'
date = 2023-12-30T16:26:23-08:00
draft = false
+++
The goal of this workshop is to create step by step application in go.

The workshop will start with basic functionality of go language. 
Gradually, it will get more intermediate and then advanced.   
