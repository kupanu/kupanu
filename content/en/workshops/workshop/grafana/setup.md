---
title: Setup
type: docs
---
In this workshop we will explorer grafan.  
The first step is to setup a grafana instance and create a dashboard with some data.  
Let's start very simple:   
- [x]The first step is to create an account   
    - [x] we will use the free account at https://www.grafana.com
    - [x] Create an account using google account 
- [x] Install/Connect data source - google sheet  
- [x] Follow the isntuction here: https://github.com/grafana/google-sheets-datasource/blob/main/src/docs/configuration.md
- [x] Click this link and it will walk through a google project creation and enable API: `https://www.googleapis.com/auth/spreadsheets.readonly`

Apparently, we neeed to have a good project setup in GCP to use in in grafan. Let's set that up:   
https://www.youtube.com/watch?v=GnWZsHjM5To   
TODO: 
- [ ] Create a GCP account 
- [ ] Create a GCP project
- [ ] Create a GCP Service Account to understand service account use cases
