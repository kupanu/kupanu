+++
title = "Hugo"
date = 2023-12-05T15:13:29-08:00
weight = 3
+++


This is the cheat sheet for hugo. Hugo is a static site generator. 

More information can be found here: https://gohugo.io/getting-started/quick-start/

Install Hugo:  
```
brew install hugo 
```
Create a site: 
```
hugo new site quickstart
cd quickstart
```
Create git repository: 
```
git init
```
Add a theme: 
```
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke
echo theme = \"ananke\" >> config.toml
```
Add some content: 
```
hugo new posts/my-first-post.md
```
Start the server
```
hugo server
```