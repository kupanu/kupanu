+++
title = 'Commands'
date = 2023-12-05T22:48:19-08:00
draft = false
+++
MKDIR

`mkdir -p parentfolder/{subfolder1,subfolder2,subfolder3}`  
`mkdir "Project "{"New Home","Vacation","To Do"}`		

Find all file with extension py  
`find . -iname '*.py' -print`  
`find / \! -name "*.c" -print'`  

Find all directory  
`find . -type d -exec ls -al {} \;`  

List all file recursively   
`ls -al -R`  
`find . -maxdepth 5 -type d`  
`find . -type f -exec ls -al {} \;`  

Find empty folder   
`find . -type d -empty -print`


DELETE/MOVE  

`sudo find . -iname "*.DS_Store" -exec rm {} \;`  
`osascript -e 'tell application "Finder" to empty trash'`  
`sudo find . -iname "*.DS_Store" -exec rm {} \;`  
`find . -type d -empty -delete`  

Delete Empty folder  
`find . -type d -empty -delete`  


Move files with some extenuation:     
`find . -iname "*.mobi" -exec mv {} . \;`  

    
Remove DS_Store  
`sudo find . -iname "*.DS_Store" -print`  
`sudo find . -iname "*.DS_Store" -exec rm {} \;`  
    
    
Remove empty directory   
`find . -type d -empty -exec rmdir {} \;`  


Install Brew    
`ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`  


Copy photos to a folder  
`find . -type f -exec mv -vn {} '/Volumes/Extreme SSD/PhotoStaging/' \; > ~/out1.txt`  

Unmount net drives  
`sudo umount -Af -t smbfs`  

Fix Camera Issue    
`sudo killall AppleCameraAssistant;sudo killall VDCAssistant`  

What port am I listing?   
`netstat -ap tcp | grep -i "listen"`  

Change ownership    
`sudo chown -R $(whoami):admin /usr/local/share`  

Brew    
`brew doctor`

Rsync
Rsync (delete src)
`rsync --dry-run --remove-source-files -azv $SRC $DEST`
`rsycn Copy  rsync  -azv $SRC $DEST`

While   

```
while aws eks describe-cluster --name "eks-launch-work
    shop" --query cluster.status --output text | grep -q CREATING; do 
	echo still createing; sleep 60; done;
```
Until   
```
until [[ `aws cloudformation describe-stacks --stack-name "eks-howto-workshop-worker-nodes" --query "Stacks[0].[StackStatus]" --output text` == "CREATE_COMPLETE" ]]; do  echo "The stack is NOT in a state of CREATE_COMPLETE at `date`";   sleep 30; done && echo "The  Stack is built at `date` - Please proceed"
```
Eval    
https://linuxhint.com/bash_eval_command/
Change Display res  
https://github.com/jhford/screenresolution.git


```
open -a "Google Chrome" --args --profile-directory=Default
/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --args --profile-directory="Profile 3"
```