+++
title = 'Multiple Accounts'
date = 2023-12-05T21:34:26-08:00
draft = false
+++
##### How to setup multiple git account in a single computer: 

I want to use multiple GitLab (or GitHub, etc.) on my machine. One could be your personal account and another one you use specifically for my startup ideas. I want to use different SSH keys to access the repositories in either account. 

Here’s how to achieve that setup properly. Let’s assume we have the two GitLab accounts:

- Profile 1: Personal Account, username: personal, email: personal@email.com
- Profile 2: Startup Account, username: startup, email: startup@email.com

##### Generate ssh keys for both profile:
`ssh-keygen -t rsa -b 2048 -C "personal"`  - name the key: personal_key  
`ssh-keygen -t rsa -b 2048 -C "startup"`   - name the key: startup_key


##### Configure SSH to use the correct keys:   
Now create the file `~/.ssh/config` and add the following contents:

```
Host gitlab.com
    HostName gitlab.com
    User git
    IdentityFile ~/.ssh/personal_key
Host gitlab.com-startup
    HostName gitlab.com
    User git
    IdentityFile ~/.ssh/startup_key
```
- Add the public key to the account: `tr -d '\n' < ~/.ssh/startup_key.pub | pbcopy`  
- Clone the repo using the: `git clone git@gitlab.com-startup:<groupname>/<projectname>.git`