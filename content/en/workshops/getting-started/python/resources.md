+++
title = 'Resources'
date = 2023-12-28T11:18:51-08:00
draft = false
weight = 1
+++

**Using PYENV:**  
https://realpython.com/intro-to-pyenv/

`brew install openssl readline sqlite3 xz zlib `  
`pyenv install --list |grep " 3\.[678]"`  


**Using Virtual ENV** (not prefered):  
https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/ 

`python -m venv .venv #Create venv `  
`source ~/env/bin/activate #Activate`  
`deactivate #Deactivate`  

`python -m venv .venv`  
`source .venv/bin/activate`  
`pip install --upgrade pip`  

`python3 -m pip install matplotlib`  
`pip3 freeze > requirements.txt`  
`pip3 install -r requirements.txt`  



`py -3 -m venv .venv`  
`.venv\scripts\activate`  





- https://docs.python-guide.org/writing/structure/
- https://pycon.switowski.com
- https://github.com/audreyfeldroy/cookiecutter-pypackage
- https://python-adv-web-apps.readthedocs.io/en/latest/index.html
