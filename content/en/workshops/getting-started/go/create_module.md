+++
title = 'Create a Go module'
date = 2023-12-30T20:35:41-08:00
draft = false
weight = 2
+++

- [Create a Module](https://go.dev/doc/tutorial/create-module)
- [Call Your Module](https://go.dev/doc/tutorial/call-module-code)
