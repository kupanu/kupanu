+++
title = 'Working With Git'
date = 2023-12-27T20:12:43-08:00
draft = false
+++

- Git init 
`git init .`
- git config
    - What .gitignore file I should create ??
		
#### Working on a branch 
- this is for new branch   
`git checkout -b blackhole/removing-feature`

- this is existing branch   
`git remote`  
`git fetch origin`  
`git branch -a`  
`git checkout -b blackhole/removing-feature`  

- work work work   
`code .`  
`git add .`  
`git commit -a -m "Message about the commit"`  
`git push -u origin blackhole/removing-uw`  
`git push --set-upstream origin blackhole/removing-feature`  
	
- Clean up local
`git checkout main`  
`git pull`  
- remote branch delete
`git pull --prune`  
- delete local branch
`git branch -d  blackhole/removing-feature`  
`git branch --all`   

- Git commit   
`git add .`  
`git commit -am "Message about the commit"`  
`git push`  

- Force git overwrite local file  
`git fetch --all`  
`git reset --hard origin/master`  
	
- If other branch   	
`git reset --hard origin/<branch_name>`
	
- Maintain current local commits
`git checkout master`  
`git branch new-branch-to-save-current-commits`   
`git fetch --all`  
`git reset --hard origin/master`   	
`git stash`   
`git stash pop`   
	
- Clean 
WARNING: this can't be undone!
`git reset --hard HEAD`  
`git clean -f -d`  
`git pull`   
	
- For dry-run 
`git clean -n -f -d`  
	
- find which remote repository it is linked to 
`git remote -v`
