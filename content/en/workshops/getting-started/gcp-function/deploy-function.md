+++
title = 'Deploy Function'
date = 2024-03-20T18:42:09-07:00
draft = false
weight = 2
+++
https://cloud.google.com/functions/docs/create-deploy-gcloud#functions_quickstart_helloworld-python  

```
gcloud functions deploy python-http-function \
--gen2 \
--runtime=python312 \
--region=us-central1 \
--source=. \
--entry-point=hello_get \
--trigger-http 
```

`gcloud functions describe python-http-function --gen2 --region REGION --format="value(serviceConfig.uri)"`

update URI with output: 

```curl -m 70 -X POST URI \
    -H "Authorization: Bearer $(gcloud auth print-identity-token)" \
    -H "Content-Type: application/json" \
    -d '{}'
```

`gcloud functions delete python-http-function --gen2 --region us-central1`